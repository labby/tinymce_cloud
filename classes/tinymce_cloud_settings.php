<?php

/**
 *  @module         TinyMCE
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2012-2024 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *  Please note: TINYMCE is distributed under the <a href="https://github.com/tinymce/tinymce/blob/develop/LICENSE.TXT">MIT License</a> 
 *
 *
 */
 
class tinymce_cloud_settings extends LEPTON_abstract implements LEPTON_wysiwyg_interface
{
	public $default_skin= "oxide-dark";
	public $default_height = "300"; // in px
	public $default_width = "100";  // in %
	public $default_toolbar = "Full";
	public $default_content_css = "document";
	
	public $skins = [];
	public $content_css = [];
	public $toolbars = [];
	public $content = "";
	
	public static $instance;
	
	public function initialize() 
	{
		$ignore_files = ["index.php"];
		
		$temp_skins  = array_slice(scanDir(LEPTON_PATH.'/modules/tinymce_cloud/tinymce/skins/ui/'), 2);
		$this->skins = array_diff($temp_skins, $ignore_files);
		
		$temp_content_css = array_slice(scanDir(LEPTON_PATH.'/modules/tinymce_cloud/tinymce/skins/content/'), 2);
		$this->content_css = array_diff($temp_content_css, $ignore_files);
		
		$this->toolbars = array(
				
			'Full'	=> "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image mailto | print preview media fullpage | code pagelink droplets",

			/**
			 *	Smart toolbar within only first two rows.
			 *
			 */
			'Smart' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | link unlink image mailto | pagelink droplets",
				
			/**
			 *	Simple toolbar within only one row.
			 *
			 */
			'Simple' => "bold italic | alignleft aligncenter alignright alignjustify | link unlink image mailto | pagelink droplets",
				
		);
		
		$this->content = "
		    <p>Hello: this is <em>TinyMCE cloud</em>.</p>
		    <p><img style='margin: 0 auto;display: block;' src='".LEPTON_URL."/modules/tinymce_cloud/icon.png' /></p>
		";	
	}
        
    // interfaces
    //  [1]
    public function getHeight()
    {
       return $this->default_height; 
    }
    //  [2]
    public function getWidth()
    {
        return $this->default_width; 
    }
    //  [3]
    public function getToolbar()
    {
        return $this->toolbars[ $this->default_toolbar ]; 
    }
    //  [4]
    public function getSkin()
    {
        return $this->default_skin ?? ""; 
    }
}	

