### tinymce_cloud

Tinymce editor from the cloud as addon for LEPTON CMS.


#### Installation Notice

Please keep in mind that you need a key to run this addon properly and to register your domain. <br />
https://www.tiny.cloud/auth/signup/

<b>Enter your personal key in the file /templates/my_key.lte into the location of 'no-api-key'.</b>

```
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

```


#### Infos

Get more information about tinymce cloud:<br />
https://www.tiny.cloud/docs/quick-start/<br />
https://www.tiny.cloud/blog/announcement-change-to-tinymce-cloud-stable





