<?php
/**
 *	Droplet's dialog
 *
 */

header("Content-type: text/javascript; charset=utf-8");

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

global $database;

$all_droplets = array();
$database->execute_query(
	"SELECT `name`,`description`,`comments` from `".TABLE_PREFIX."mod_droplets` ORDER By `name`",
	true,
	$all_droplets
);

function droplets_cleanup_str(&$str) {
	$str = addslashes($str);
	$str = str_replace(
		array("\n", "\n\r", "\r"),
		"\\n",
		$str
	);
}

$droplets_values = "var dropletsvalues = [\n";
$droplets_info = "var dropletsinfo = [\n";

foreach($all_droplets as &$d){
	droplets_cleanup_str($d['description']);
	droplets_cleanup_str($d['comments']);
	
	$droplets_values .= "{ text:'".$d['name']."', value:'".$d['name']."'},\n";
	$droplets_info .= "{ text:'".$d['name']."', desc:'".$d['description']."', comment:'".$d['comments']."'},\n";
}

$droplets_values = substr($droplets_values, 0, -2)."\n];\n";
$droplets_info = substr($droplets_info, 0, -2)."\n];\n";

echo $droplets_values;
echo $droplets_info;

?>