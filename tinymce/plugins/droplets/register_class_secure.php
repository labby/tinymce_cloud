<?php

/**
 *  @module         TinyMCE
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2010-2024 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        GPLv2+ License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *  Please note: TINYMCE is distributed under the <a href="https://github.com/tinymce/tinymce/blob/develop/LICENSE.TXT">MIT License</a> 
 *
 *
 */


$files_to_register = array(
	'droplets.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );
?>