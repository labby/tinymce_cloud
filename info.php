<?php

/**
 *  @module         TinyMCE-Cloud
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2012-2024 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *  Please note: TINYMCE is distibuted under the <a href="https://github.com/tinymce/tinymce/blob/develop/LICENSE.TXT">GNU Lesser General Public License v2.1</a>
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory     = 'tinymce_cloud';
$module_name          = 'TinyMCE-Cloud';
$module_function      = 'WYSIWYG';
$module_version       = '4.0.0';  // tinymce 7-series
$module_platform      = '7.x';
$module_author        = 'erpe, Aldus';
$module_home          = 'https://lepton-cms.com';
$module_guid          = '0ad7e8dd-2f6b-4525-b4bf-db326b0f5ae8';
$module_license       = 'GNU General Public License, TINYMCE is LGPL';
$module_license_terms  = '-';
$module_description   = '<a href="https://www.tiny.cloud" target="_blank">Current TinyMCE </a>allows you to edit the content<br />of a page and see media image folder. This release needs a Tinymce API-Key!';

// Notice: https://www.tiny.cloud/docs/tinymce/latest/invalid-api-key/